import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio2App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<Double> precios = new ArrayList<>();
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce cantidad de productos: ");
		int cantidadCarrito = entrada.nextInt();//PEDIMOS QUE INTRODUZCA CUANTOS PRODUCTOS TIENE EL CARRO
		int contadorProductos = 0;//CONTADOR PARA SABER CUANTOS PRODUCTOS HEMOS PUESTO EL PRECIO
		while(cantidadCarrito!=contadorProductos) {
			System.out.print("Introduce precio del producto "+contadorProductos+" :");
			double precioProducto = entrada.nextDouble();
			precios.add(precioProducto);//VAMOS AGREGANDO LOS PRECIOS AL ARRAYLIST									
			contadorProductos++;
		}
		
		double totalPrecio = sumaPrecios(precios);//LLAMAMOS AL METODO SUMARPRECIOS PARA QUE SUME TODO LO QUE HAY DENTRO DEL ARRAYLIST Y LO ALMACEAMOS EN UNA VARIABLE
		System.out.println();
		System.out.print("Introduce el IVA a aplicar:");
		int iva = entrada.nextInt();//INTRODUCIMOS 4 O 21 DE IVA
		double precioConIva = calcularIVA(totalPrecio, iva);//LAMAMOS AL METODO DE CALCULARIVA PARA QUE NOS CALCULE EL PRECIO FINAL CON EL IVA
		System.out.println("Precio total: "+precioConIva);
		double pago = Pago(precioConIva, entrada);//LLAMAMOS AL METODO PAGO PARA SABER CUANTO HA PAGADO UN USUARIO
		double cambio = cambioDevolver(pago, precioConIva, entrada);//LLAMAMOS AL METODO cambioDevolver PARA SABER CUANTO HAY QUE DEVOLVER AL USUARIO
		mostrarInformacion(iva, totalPrecio, precioConIva, cantidadCarrito, pago, cambio);//LAMAMOS AL METODO PARA QUE MUESTRE TODA LA INFORMACION
		
	
		

	}
	
	public static double sumaPrecios(ArrayList<Double> precios) {
		System.out.println("Precios de los productos");
		double totalPrecio = 0;
		for(Double precio:precios) {
			System.out.print(precio+" ");
			totalPrecio = totalPrecio + precio;
			
		}
		
		return totalPrecio;
	}
	//METODO QUE CALCULA EL IVA DEPENDIENDO SI INTRODUCE 4 O 21
	public static double calcularIVA(double totalPrecio, int iva) {
		Scanner entrada = new Scanner(System.in);
		double precioConIva = 0;
		boolean correcto = false;
		while(!correcto) {
					
		switch (iva) {
		case 4:
		 precioConIva = totalPrecio * 0.04;
		 precioConIva = precioConIva + totalPrecio;
		 correcto = true;

			break;
		case 21:
		 precioConIva = totalPrecio * 0.21;
		 precioConIva = precioConIva + totalPrecio;
		 correcto = true;
			break;

		default:
			System.out.println("No existe ese IVA");
			System.out.print("Introduce IVA: ");
			iva = entrada.nextInt();
			break;
		}
		}
		return precioConIva;
	}
	
	//PEDIMOS AL USUARIO QUE INTRODUZCA SU PAGO
	public static double Pago(double precioConIva, Scanner entrada) {
		boolean pagado = false;	
		System.out.print("Introduce pago: ");
		double pago = entrada.nextDouble();
		while(!pagado) {
			if(pago>precioConIva) {
				pagado = true;
			}else {
				System.out.print("PAGO INSUFICIENTE, POR FAVOR INTRODUZCA MAS DINERO: ");
				pago = entrada.nextDouble();
			}
	
	}
		return pago;
	}
	
	//METODO PARA CALCULAR EL CAMBIO
	public static double cambioDevolver(double pago, double precioConIva, Scanner entrada) {
		double cambio = 0;
		cambio = pago - precioConIva;
					
		return cambio;
	}
	
	
	//METODO PARA MOSTRAR TODA LA INFORMACION POR PANTALLA
	public static void mostrarInformacion(int iva, double totalPrecio, double precioConIva, int cantidadCarrito, double pago, double cambio) {
		System.out.println();
		System.out.println("---------------RECIBO----------------------------");
		System.out.println();
		System.out.println("IVA Aplicado: "+iva);
		System.out.println("Precio total bruto: "+totalPrecio);
		System.out.println("Precio mas IVA: "+precioConIva);
		System.out.println("Articulos comprados: "+cantidadCarrito);
		System.out.println("Cantidad pagada: "+pago);
		System.out.println("Cambio a devolver: "+cambio);
		
	}

}
