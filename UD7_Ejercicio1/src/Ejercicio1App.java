import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio1App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);	
		System.out.print("Cuantos alumnos quieres introducir notas:"); //PEDIMOS EL NUMERO DE ALUMNOS
		int nAlumnos = entrada.nextInt();
		int contador = 0;
		while(nAlumnos>contador) {
			Random numero = new Random();
			ArrayList<Integer> notas=new ArrayList<>();
			//AGREGAMOS NOTAS RANDOM PARA HACER PRUEBAS
			notas.add(numero.nextInt(11));
			notas.add(numero.nextInt(11));
			notas.add(numero.nextInt(11));
			notas.add(numero.nextInt(11));			
			System.out.println("---------------NOTAS USUARIO "+contador+"-----------------");						
			Hashtable<Integer,Integer> medias=new Hashtable<Integer,Integer>();
			int media = generarNotaMedia(notas); //LLAMAMOS AL METODO QUE GENERA UNA NOTA MEDIA Y LO INGRESAMOS EN UNA VARIABLE

			medias.put(contador,media);

			mostrarDatos(medias);

			contador++;

		}

		

	
		
		
		
		
	}
	
	//METODO QUE SACA LA NOTA MEDIA DEL ArrayList
	public static int generarNotaMedia(ArrayList<Integer> notas) {
		int sumaNotas = 0;
		int notaMedia = 0;
		for(Integer nota:notas) {
			System.out.print(nota+" ");
			sumaNotas = sumaNotas + nota;//VAMOS SUMANDO TODAS LAS NOTAS DEL ARRAYLIST
			
		}
		notaMedia = sumaNotas/4;//HACEMOS LA DIVISION POR EL NUMERO DE NOTAS Y TENEMOS LA NOTA MEDIA

		return notaMedia;
	}
	
	//METODO PARA MOSTRAR TODOS LOS DATOS POR PANTALLA
	public static void mostrarDatos(Hashtable<Integer,Integer> medias) {
		Enumeration<Integer> notasMedia = medias.elements();
		Enumeration<Integer> claveUsuario = medias.keys();
		System.out.println();
		System.out.println("Notas medias:");
		while(claveUsuario.hasMoreElements()) {
			System.out.print("Clave Usuario: "+claveUsuario.nextElement());
			System.out.println("   Nota media: "+notasMedia.nextElement());
		}
	}

}
