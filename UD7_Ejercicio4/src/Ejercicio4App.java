import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Hashtable<String,Double> articulos=new Hashtable<String,Double>();
		ArrayList<Double> precios = new ArrayList<>();
		articulosPorDefecto(articulos);//AGREGAMOS LOS ARTICULOS POR DEFECTO
		Scanner entrada = new Scanner(System.in);
		//MENU DEL USUARIO
        boolean sortir = false;
        do {
            int opcion = Menu();

            switch (opcion) {
                case 1:
                	agregarArticulo(articulos, entrada);
                    break;

                case 2:
                	buscarArticulo(articulos, entrada);
                    break;
                case 3:
                	eliminarArticulo(articulos, entrada);
                    break;
                    
                case 4:
                	mostrarTodaLaInformación(articulos);
                    break;
                case 5:
            		System.out.print("Introduce cantidad de productos: ");
            		int cantidadCarrito = entrada.nextInt();//PEDIMOS QUE INTRODUZCA CUANTOS PRODUCTOS TIENE EL CARRO
            		int contadorProductos = 0;//CONTADOR PARA SABER CUANTOS PRODUCTOS HEMOS PUESTO EL PRECIO
            		while(cantidadCarrito!=contadorProductos) {
            			System.out.print("Introduce nombre del articulo:");
            			String nombreArticulo = entrada.next();
            			double precioArticulo = buscarPrecioArticulo(articulos, entrada, nombreArticulo);//GUARDAMOS EL PRECIO DEL ARTICULO INTRODUCIDO
            			precios.add(precioArticulo);//VAMOS AGREGANDO LOS PRECIOS AL ARRAYLIST	
            			contadorProductos++;
            		}
            		double totalPrecio = sumaPrecios(precios);//LLAMAMOS AL METODO SUMARPRECIOS PARA QUE SUME TODO LO QUE HAY DENTRO DEL ARRAYLIST Y LO ALMACEAMOS EN UNA VARIABLE
            		System.out.println();
            		System.out.print("Introduce el IVA a aplicar:");
            		int iva = entrada.nextInt();//INTRODUCIMOS 4 O 21 DE IVA
            		double precioConIva = calcularIVA(totalPrecio, iva);//LAMAMOS AL METODO DE CALCULARIVA PARA QUE NOS CALCULE EL PRECIO FINAL CON EL IVA
            		System.out.println("Precio total: "+precioConIva);
            		double pago = Pago(precioConIva, entrada);//LLAMAMOS AL METODO PAGO PARA SABER CUANTO HA PAGADO UN USUARIO
            		double cambio = cambioDevolver(pago, precioConIva, entrada);//LLAMAMOS AL METODO cambioDevolver PARA SABER CUANTO HAY QUE DEVOLVER AL USUARIO
            		mostrarInformacion(iva, totalPrecio, precioConIva, cantidadCarrito, pago, cambio);//LAMAMOS AL METODO PARA QUE MUESTRE TODA LA INFORMACION
            		
                    break;

                case 6:
                    System.out.println("FIN PROGRAMA");
                    sortir = true;
                    break;

                default:
                    sortir = true;

            }

        } while (!sortir);

	}
    public static int Menu() {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Seleccionar opción");
        System.out.println("­­­­­­­­­­­­­­­­­­­­­­­­­­­­");
        System.out.println("[1] Introducir articulo y precio");
        System.out.println("[2] Buscar información articulo");
        System.out.println("[3] Eliminar articulo");
        System.out.println("[4] Listar toda la información");
        System.out.println("[5] Comprar articulos");
        System.out.println("[6] Sortir");
        System.out.println("");
        System.out.println("Introducir opció: ");
        int opcion = entrada.nextInt();
        return opcion;

    }
    
    public static void eliminarArticulo(Hashtable<String,Double> articulos, Scanner entrada) {
    	System.out.print("Introduce nombre del articulo a eliminar: ");
    	String nombreArticulo = entrada.next();
    	if(articulos.remove(nombreArticulo) == null) {
    		System.out.println("NO SE ENCUENTRA ESTE ARTICULO");
    		System.out.println();
    	}else {
    		articulos.remove(nombreArticulo);
    	}
    	
    	
    }
    

    
	public static double sumaPrecios(ArrayList<Double> precios) {
		System.out.println("Precios de los productos");
		double totalPrecio = 0;
		for(Double precio:precios) {
			System.out.print(precio+" ");
			totalPrecio = totalPrecio + precio;
			
		}
		
		return totalPrecio;
	}
	
	//METODO QUE CALCULA EL IVA DEPENDIENDO SI INTRODUCE 4 O 21
	public static double calcularIVA(double totalPrecio, int iva) {
		Scanner entrada = new Scanner(System.in);
		double precioConIva = 0;
		boolean correcto = false;
		while(!correcto) {
					
		switch (iva) {
		case 4:
		 precioConIva = totalPrecio * 0.04;
		 precioConIva = precioConIva + totalPrecio;
		 correcto = true;

			break;
		case 21:
		 precioConIva = totalPrecio * 0.21;
		 precioConIva = precioConIva + totalPrecio;
		 correcto = true;
			break;

		default:
			System.out.println("No existe ese IVA");
			System.out.print("Introduce IVA: ");
			iva = entrada.nextInt();
			break;
		}
		}
		return precioConIva;
	}
	
	//PEDIMOS AL USUARIO QUE INTRODUZCA SU PAGO
	public static double Pago(double precioConIva, Scanner entrada) {
		boolean pagado = false;	
		System.out.print("Introduce pago: ");
		double pago = entrada.nextDouble();
		while(!pagado) {
			if(pago>precioConIva) {
				pagado = true;
			}else {
				System.out.print("PAGO INSUFICIENTE, POR FAVOR INTRODUZCA MAS DINERO: ");
				pago = entrada.nextDouble();
			}
	
	}
		return pago;
	}
	
	//METODO PARA CALCULAR EL CAMBIO
	public static double cambioDevolver(double pago, double precioConIva, Scanner entrada) {
		double cambio = 0;
		cambio = pago - precioConIva;			
		
		return cambio;
	}
	
	//METODO PARA MOSTRAR TODA LA INFORMACION POR PANTALLA
	public static void mostrarInformacion(int iva, double totalPrecio, double precioConIva, int cantidadCarrito, double pago, double cambio) {
		System.out.println();
		System.out.println("---------------RECIBO----------------------------");
		System.out.println();
		System.out.println("IVA Aplicado: "+iva);
		System.out.println("Precio total bruto: "+totalPrecio);
		System.out.println("Precio mas IVA: "+precioConIva);
		System.out.println("Articulos comprados: "+cantidadCarrito);
		System.out.println("Cantidad pagada: "+pago);
		System.out.println("Cambio a devolver: "+cambio);
		
	}
    
    //ARTICULOS QUE ESTARAN POR DEFECTO EN EL DICCIONARIO
    public static void articulosPorDefecto(Hashtable<String,Double> articulos) {
    	articulos.put("Portatil HP",400.0);
    	articulos.put("Portatil Asus",350.0);
    	articulos.put("Raton",10.0);
    	articulos.put("Teclado Trust",20.0);
    	articulos.put("Pantalla Asus",100.0);
    	articulos.put("Altavoz",50.50);
    	articulos.put("Disco duro Externo",45.45);
    	articulos.put("Raton Logitech",55.0);
    	articulos.put("Samsung Galaxy TAB A",220.12);
    	articulos.put("Auriculares Mi",20.12);

    }
    
    //METODO QUE MUESTRA EL LISTADO COMPLETO 
    public static void mostrarTodaLaInformación(Hashtable<String,Double> articulos) {
    	System.out.println("--------------------------------------------------");
    	System.out.println("			LISTADO COMPLETO");
    	System.out.println("--------------------------------------------------");
		Enumeration<Double> precio = articulos.elements();
		Enumeration<String> articulo = articulos.keys();
		
		while(articulo.hasMoreElements()) {
			System.out.println("Articulo: "+articulo.nextElement());
			System.out.println("Precio: "+precio.nextElement());
			System.out.println();
		}
    }
    
    //METODO PARA AGREGAR ARTICULOS NUEVOS AL DICCIONARIO
    public static void agregarArticulo(Hashtable<String,Double> articulos, Scanner entrada) {
    	System.out.println("--------------------------------------------------");
    	System.out.println("			AGREGAR ARTICULO");
    	System.out.println("--------------------------------------------------");
    	System.out.print("Introduce nombre del articulo: ");
    	String nombreArticulo = entrada.next();
    	System.out.print("Introduce el precio del articulo: ");
    	double precioArticulo = entrada.nextDouble();
    	articulos.put(nombreArticulo,precioArticulo);
    	System.out.println("--------------------------------------------------");
    	System.out.println("			AGREGADO CON EXITO");
    	System.out.println("--------------------------------------------------");

    }
    
    //METODO PARA BUSCAR UN ARTICULO EN CONCRETO EN EL DICCIONARIO Y QUE MUESTRE SU PRECIO
    public static void buscarArticulo(Hashtable<String,Double> articulos, Scanner entrada) {
    	System.out.println("--------------------------------------------------");
    	System.out.println("			BUSCAR ARTICULO");
    	System.out.println("--------------------------------------------------");
    	System.out.print("Introduce nombre del articulo: ");
    	String articuloBuscar = entrada.next();
		System.out.println();
    	if(articulos.get(articuloBuscar) == null) {//SI NO ENCUENTRA EL NOMBRE DEL ARTICULO
    		System.out.println("NO SE HA ENCONTRADO EL ARTICULO");
    	}else {
        	System.out.println("El precio de "+articuloBuscar+" es: "+articulos.get(articuloBuscar));    
  
}
    }
    
    //METODO PARA DEVOLVER PRECIO DE UN ARTICULO INTRODUCIDO POR EL USUARIO
    public static double buscarPrecioArticulo(Hashtable<String,Double> articulos, Scanner entrada, String nombreArticulo) {
    	double precioArticulo = 0;
    	boolean encontrado = false;
    	while(!encontrado) {
    		if(articulos.get(nombreArticulo) == null) {//SI NO ENCUENTRA EL NOMBRE DEL ARTICULO
        		System.out.println("NO SE HA ENCONTRADO EL ARTICULO");
        		System.out.print("Introduce nombre del articulo: ");
        		nombreArticulo = entrada.next();
        	}else {
        		 precioArticulo = articulos.get(nombreArticulo);
        		 encontrado = true;

    }
    	}
    
    	return precioArticulo;

    }
}
