import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Hashtable<String,Integer> articulos=new Hashtable<String,Integer>();
		articulosPorDefecto(articulos);
		Scanner entrada = new Scanner(System.in);
		//MENU DEL USUARIO
        boolean sortir = false;
        do {
            int opcion = Menu();

            switch (opcion) {
                case 1:
                	agregarArticulo(articulos, entrada);
                    break;

                case 2:
                	buscarArticulo(articulos, entrada);
                    break;
                case 3:
                   mostrarTodaLaInformación(articulos);
                    break;
                case 4:
                    System.out.println("FIN PROGRAMA");
                    sortir = true;
                    break;

                default:
                    sortir = true;

            }

        } while (!sortir);
    }

	//OPCIONES DEL MENU
    public static int Menu() {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Seleccionar opción");
        System.out.println("­­­­­­­­­­­­­­­­­­­­­­­­­­­­");
        System.out.println("[1] Introducir articulo y precio");
        System.out.println("[2] Buscar información articulo");
        System.out.println("[3] Listar toda la información");
        System.out.println("[4] Sortir");
        System.out.println("");
        System.out.println("Introducir opció: ");
        int opcion = entrada.nextInt();
        return opcion;

    }
    
    //ARTICULOS QUE ESTARAN POR DEFECTO EN EL DICCIONARIO
    public static void articulosPorDefecto(Hashtable<String,Integer> articulos) {
    	articulos.put("Portatil HP",400);
    	articulos.put("Portatil Asus",350);
    	articulos.put("Raton",10);
    	articulos.put("Teclado Trust",20);
    	articulos.put("Pantalla Asus",100);
    	articulos.put("Altavoz",50);
    	articulos.put("Disco duro Externo",45);
    	articulos.put("Raton Logitech",55);
    	articulos.put("Samsung Galaxy TAB A",220);
    	articulos.put("Auriculares Mi",20);

    }
    
    //METODO QUE MUESTRA EL LISTADO COMPLETO 
    public static void mostrarTodaLaInformación(Hashtable<String,Integer> articulos) {
    	System.out.println("--------------------------------------------------");
    	System.out.println("			LISTADO COMPLETO");
    	System.out.println("--------------------------------------------------");
		Enumeration<Integer> precio = articulos.elements();
		Enumeration<String> articulo = articulos.keys();
		
		while(articulo.hasMoreElements()) {
			System.out.println("Articulo: "+articulo.nextElement());
			System.out.println("Precio: "+precio.nextElement());
			System.out.println();
		}
    }
    
    //METODO PARA AGREGAR ARTICULOS NUEVOS AL DICCIONARIO
    public static void agregarArticulo(Hashtable<String,Integer> articulos, Scanner entrada) {
    	System.out.println("--------------------------------------------------");
    	System.out.println("			AGREGAR ARTICULO");
    	System.out.println("--------------------------------------------------");
    	System.out.print("Introduce nombre del articulo: ");
    	String nombreArticulo = entrada.next();
    	System.out.print("Introduce el precio del articulo: ");
    	int precioArticulo = entrada.nextInt();
    	articulos.put(nombreArticulo,precioArticulo);
    	System.out.println("--------------------------------------------------");
    	System.out.println("			AGREGADO CON EXITO");
    	System.out.println("--------------------------------------------------");

    }
    
    //METODO PARA BUSCAR UN ARTICULO EN CONCRETO EN EL DICCIONARIO Y QUE MUESTRE SU PRECIO
    public static void buscarArticulo(Hashtable<String,Integer> articulos, Scanner entrada) {
    	System.out.println("--------------------------------------------------");
    	System.out.println("			BUSCAR ARTICULO");
    	System.out.println("--------------------------------------------------");
    	System.out.print("Introduce nombre del articulo: ");
    	String articuloBuscar = entrada.next();
		System.out.println();
    	if(articulos.get(articuloBuscar) == null) {//SI NO ENCUENTRA EL NOMBRE DEL ARTICULO
    		System.out.println("NO SE HA ENCONTRADO EL ARTICULO");
    	}else {
        	System.out.println("El precio de "+articuloBuscar+" es: "+articulos.get(articuloBuscar));    
        	

    	}
    	System.out.println();
    }
	

}
